;(function(){

  var socket = io.connect();

  $('#file-input').change(function(evt){
    var file = evt.target.files[0];
//    console.log(file);
    upFile(file);
  });

  var upFile = function(aFile) {

    // FileAPIを使う
    var fileReader = new FileReader();
    var sendFile = aFile;

    var data = {};
    // バイナリ形式でファイルを読み込む
    fileReader.readAsBinaryString(sendFile);
    fileReader.onload = function(evt) {
      data.file = evt.target.result;
      data.name = 'uploadFile';
      socket.json.emit('upload', data);
    };
    fileReader.error = function() {
      console.log('ファイルの読み込み失敗');
    };

  };

})();