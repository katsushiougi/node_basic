var
  http = require('http'),
  fs   = require('fs'),
  path = require('path'),
  // socket.io が一番機能が豊富
  io   = require('socket.io');

var server = http.createServer();

server.on('request', function(req, res){
  var lookup = path.basename( decodeURI(req.url) ) || 'index.html';
  var f      = lookup;
  // ファイルの存在確認
  fs.exists(f, function(exists){
    if (exists) {
      fs.readFile(f, function(err, data){
        if (err) {
          res.writeHead(500);
          res.end('Server Error');
          return;
        }
        res.writeHead(200, {'Content-type': 'text/html'});
        res.end(data);
      });
    }
  });
}).listen(3000, 'localhost');

var server_io = io.listen(server);

server_io.sockets.on('connection', function(socket){

  // イベント名はインターフェイス仕様書を作って管理する
  socket.on('message_from_client', function(data){
    // console.log(data);
    /**
     * コネクションを貼ったらユニークソケットIDが付与さえる
     */
    // 自分だけへ送信
    socket.emit('msg_from_server', data + 'さきこ');
    // 接続している自分以外の人へのメッセージ送信
    socket.broadcast.emit('msg_from_server', data + 'さきこ');
    // 基本的にはひとつの文字列しか送れないが、json.emitでjsonで返せる。
    var obj = {name: 'むらおか', age: '32'};
    socket.json.emit('json_from_server', obj);
  });

});