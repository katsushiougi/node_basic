
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
// __dirname: カレントディレクトリ
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

/*
* この順番は重要
* ここがミドルウェアと呼ばれるところ
* 一個づつ配列にプッシュしているイメージ
* 独自ミドルウェアを実行させたりもできる
* */
// Expressのデフォルトファビコンを表示
app.use(express.favicon());
// 開発用のログを表示させる
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
//app.use(express.session()); // セッションを使いたいとき
app.use(express.methodOverride());
// スタックを積んでくれる
// app.routerは暗黙的にnextを呼ぶ
app.use(app.router);
// index.htmlとか静的ファイルは直接publicに置く
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

//app.get('/', routes.index);
//app.get('/users', user.list);

//app.get('/', function(req, res){
//  res.send('<h1>タイトル</h1>');
//});

app.get('/', function(req, res){
  ///Users/katsushiOUGI/Documents/trial_node/0216-5
//  console.log(__dirname);
  res.sendfile(__dirname + '/public/index.html');
});

app.get('/:name', function(req, res){
//  console.log(req.params.name);
  res.send('<h1>' + req.params.name + '</h1>');
});


http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
