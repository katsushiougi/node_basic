
/*
 * GET home page.
 */

//exports.index = function(req, res){
//
//  // セッションがreqに入ってくる
//  console.log(req.session);
//
//  // セッションを保存
//  if (req.session) {
//    console.log(req.session);
//    req.session.user = {
//      user: 'muraoka',
//      pwd : 'sakiko'
//    };
//  }
//  res.render('index', { title: 'Express' });
//};

// ログインパスフレーズをグロバールで保持
var user = {name: 'muraoka', pwd: 'sakiko'};

exports.login = function(req, res, next) {
  var param = req.body.user;
  console.log(param);
  if (param) {
    // パスワードとの比較
    if ( (user.name === param.name) && (user.pwd === param.pwd) ) {
      // セッションに保存
      req.session.user = {
        name: user.name,
        pwd : user.psw
      };
      res.render('index', {title: 'Express', user: req.session.user});
    }
  }
  // 存在しない場合はNext
  next();
};

// ログアウト
exports.logout = function(req, res, next) {
  console.log('logout');
  // セッションを切る
  delete req.session.user;
  // 次の処理へ移動
  next();
};

exports.index = function(req, res) {
  console.log('index');
  // セッション情報を渡す
  res.render('index', {title: 'Express', user: req.session.user});
};











