
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

// Sessionオプションで追加されたモジュールは以下2行
// 引数: 署名付きCookieを生成するための秘密文字列(パスフレーズ)
// app.routerの前に書かないと動かないので注意
app.use(express.cookieParser('muraoka'));
app.use(express.session());

app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);

// POSTリクエストのときは、routes.login -> routes.indexの順番で動く
app.post('/', routes.login, routes.index);

// DELETEリスクエストのときは、routes.logout -> routes.indexの順に動作
app.del('/', routes.logout, routes.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
