var http = require('http');
var path = require('path');

var server = http.createServer();

var pages = [
  {route: '/', output: 'Hello'},
  {route: '/about', output: 'Helloほげほげ'},
  {route: '/about/email', output: 'Hello Email'}
];

server.on('request', function(req, res){

  var lookup = decodeURI(req.url);

  // /about/email で email を取得したい
  var lookup2 = path.basename( decodeURI(req.url) );
  console.log(lookup2);

  pages.forEach(function(page){
    if (lookup === page.route) {
      // 文字化けする場合はcharsetを指定する
      res.writeHead('200', {'Content-Type':'text/html; charset=utf-8'});
//      res.write(page.output);
      // 忘れないように
      // res.end にも書ける
      res.end(page.output);
    }
  });

  // パスが存在しないときのルート
  // res.finished は res.end が呼ばれなかった場合
  // 404以外に使うケース少ない
  if(!res.finished) {
    res.writeHead('404', {'Content-Type':'text/html'});
    res.end('<h1>Page Not Found</h1>');
  }

}).listen(3000, 'localhost');