var
  http = require('http'),
  fs   = require('fs'),
  path = require('path'),
  // socket.io が一番機能が豊富
  io   = require('socket.io');

var server = http.createServer();

server.on('request', function(req, res){
  var lookup = path.basename( decodeURI(req.url) ) || 'index.html';
  var f      = lookup;
  // ファイルの存在確認
  fs.exists(f, function(exists){
    if (exists) {
      fs.readFile(f, function(err, data){
        if (err) {
          res.writeHead(500);
          res.end('Server Error');
          return;
        }
        res.writeHead(200, {'Content-type': 'text/html'});
        res.end(data);
      });
    }
  });
}).listen(3000, 'localhost');

var server_io = io.listen(server);

server_io.sockets.on('connection', function(socket){

});

/**
 * ネームスペース
 * コードが見やすくなる
 */

// 名前空間: about
var about = server_io.of( '/about' );
about.on('connection', function(socket){
  socket.on('msg_from_client', function(data){
    console.log(data);
  });
});

// 名前空間: news
var news = server_io.of('/news');
news.on('connection', function(socket){
  socket.on('msg_from_client', function(data){
    console.log(data);
  });
});
