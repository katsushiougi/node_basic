var
  http = require('http'),
  fs   = require('fs'),
  path = require('path'),
  // socket.io が一番機能が豊富
  io   = require('socket.io');

var server = http.createServer();

server.on('request', function(req, res){
  var lookup = path.basename( decodeURI(req.url) ) || 'index.html';
  var f      = lookup;
  // ファイルの存在確認
  fs.exists(f, function(exists){
    if (exists) {
      fs.readFile(f, function(err, data){
        if (err) {
          res.writeHead(500);
          res.end('Server Error');
          return;
        }
        res.writeHead(200, {'Content-type': 'text/html'});
        res.end(data);
      });
    }
  });
}).listen(3000, 'localhost');

var server_io = io.listen(server);

server_io.sockets.on('connection', function(socket){

  socket.on('msg_from_client', function(obj){
    /**
     * 接続した人固有のデータを保存する
     */
    // console.log(data);
    // データをソケットに紐づけて設定
    // 'name_set'はキーのイメージで考える
//    socket.set('name_set', data);
//    // 設定したデータの取得
//    socket.get('name_set', function(err, data){
//      console.log(data);
//    });
//      console.log(JSON.stringify(obj));
//      socket.set('user_name', obj.name);

      // 個別のルームに入る
      socket.join(obj.room);
      socket.set('user_name', obj.name);

      socket.emit('name_regist_from_server', 'OK');

  });

  socket.on('msg2_from_client', function(data){

    socket.get('user_name', function(err, name){

      socket.get('user_room', function(err, roomName){
//        console.log('XXXX:' + roomName);
        // room毎に設定
        var sendMsg = '[' + roomName + ':' + name + '] ' + data;
        socket.emit('msg2_from_server', sendMsg);
        // ルーム内の他のメンバにブロードキャスト
        socket.broadcast.to(roomName).emit('msg2_from_server', sendMsg);

      });

    });
  });

});