var http = require('http');
var path = require('path');
// 起動時にファイルを読み込んでいる
var form = require('fs').readFileSync('index.html');
var qs   = require('querystring');
var util = require('util');

var server = http.createServer();

server.on('request', function(req, res){

  // GETは大文字じゃないとダメ
  if(req.method === 'GET') {
    res.writeHead('200', {'Content-Type': 'text/html'});
    res.end(form);
  }

  // POST処理
  if(req.method === 'POST') {
    var postData = '';
    req.on('data', function(chunk){
      // chunkが実際のデータ
      // console.log(chunk);
      // データが重い時に複数回dataイベントが発生する
      postData += chunk;
    }).on('end', function(){
      // データ取得後
      // オブジェクト形式でデータを取得できる
      var postDataObj = qs.parse(postData);
      console.log(postDataObj);
//      console.log(postDataObj.userinput1);
      console.log('POSTされたデータは' + postData);
      res.end(
        'あなたがPOSTしたデータは' +
         postDataObj.userinput1 + ' ' +
         postDataObj.userinput2);
    });
  }

}).listen(3000, 'localhost');
