var http = require('http');
var fs   = require('fs');
var path = require('path');

var server = http.createServer();

var mimeType = {
  '.js'  : 'text/javascript',
  '.html': 'text/html',
  '.css' : 'text/css',
  '.jpg' : 'image/jpeg'
};

server.on('request', function(req, res){

  var lookup = path.basename(decodeURI(req.url)) || 'index.html';

  // ファイルが存在したら配信する
  fs.exists(lookup, function(exists){
    if(exists) {
      fs.readFile(lookup, function(err, data){
        if (err) {
          // ファイルが読み込めない場合はサーバーのエラー
          res.writeHead('500');
          res.end('Server Error!!');
          return;
        }
        else {
          // 静的ファイル配信
          var header = {'content-Type': mimeType[path.extname(lookup)]};
          res.writeHead(200, header);
          res.end(data);
        }
      });
    }
  });

}).listen(3000, 'localhost');