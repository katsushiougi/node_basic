var http   = require('http');
var server = http.createServer();

server.on('request', function(req, res){
  // コールバックなのでリクエストがあったら実行される
  if (req.url === '/favicon.ico') {
    res.end();
    return;
  }
  console.log('接続したよ');
  // コンテントタイプを指定してブラウザに知らせる
  // プロトコル: 通信規約 200がOKというのはだいたい同じ
  res.writeHead('200', {'Content-Type':'text/html'});
  res.write('<h1>タイトル!</h1>');
  res.end();
}).listen(3000, 'localhost');