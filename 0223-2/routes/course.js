exports.index = function(req, res) {
//  res.send('<h1>講座案内</h1>');
  var courseName = req.params.courseName;
  if (!courseName) {
    var courseObj = [
      {courseName: 'ほんきでJavaScript', link: '/'},
      {courseName: 'WEBアプリ講座', link: '/'},
      {courseName: 'Node.js講座', link: '/course/nodejs'},
      {courseName: 'Unity講座', link: '/'}
    ];
    res.render('course', {title: '講座案内', courseList: courseObj});
  }
  else if(courseName === 'nodejs') {
    res.render('nodejs', {title: 'Node.js講座'});
  }
};