
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
/**
 * ミドルウェア
 * スタック構造になっている(上から順の処理)
 * 独自のミドルウェアを追加する
 */
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());

// ルーティング
// スタックを積んでくれる(順番に実行するイベントを登録する)
// app.routerは暗黙的にnextを呼ぶ
// なぜなら後にスタティックファイルを呼ぶメソッドが控えている
// ソースコードを読むといい
app.use(app.router);

// スタティックファイル
app.use(express.static(path.join(__dirname, 'public')));

// オリジナルのミドルウェア
//app.use(function(req, res, next){
////  res.send('<h1>そんなページはないよ(ミドルウェア)</h1>');
//  next();
//});
//
//app.use(function(req, res){
//  // res.sendをすればそこで終わる
//  res.send('<h1>そんなページはないよ(ミドルウェア2)</h1>');
//});

// POSTとGET


// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// app.get('/', routes.index);
// app.get('/users', user.list);

/**
 * Redis: 簡単なDB
 * 簡単なウェブゲーム向け
 * メモリに保存する
 * saveコマンドでHDに保存
 */

var userName = '';
app.post('/', function(req, res, next){
  userName = req.body.userName;
  console.log(userName);
  res.end();
});

app.get('/', function(req, res, next){
  // レスポンス用のオブジェクト
  var resMsg = {};
  resMsg.name = userName;
  resMsg.comment = 'こんにちは';
  res.end(JSON.stringify(resMsg));
});



/**
 * app.routerの内容
 * 1リクエストに対して1レスポンスが無いと固まってしまう
 */
// 正規表現が使える
// app.router
//app.get('/:contents/:id?', function(req, res, next){
//  console.log(req.params.contents);
//  console.log(req.params.id);
//  var contents = req.params.contents;
//  var id = req.params.id;
//  // == を使う場合はコメントに理由を残す
//  if(contents === 'about'){
//    if(!id) {
//      res.send('<h1>aboutページだよ</h1>');
//    }
//    else if(id === '1'){
//      res.send('<h1>詳細ページだよ</h1>');
//    }
//    else {
////      res.send('<h1>そんなページは無いよ</h1>');
////      次のミドルウェアに移行する
////      明示的にわかるようにnextを書く
////      next();
//    }
//  }
//  else {
////    res.send('<h1>そんなページは無いよ</h1>');
////    次のミドルウェアに移行する
////    next();
//  }
//  // レスポンスを一回も返していない
//  // res.finished は res.end が呼ばれなかった場合
//  // !で終わらなかった場合
//  if (!res.finished) next();
//});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
